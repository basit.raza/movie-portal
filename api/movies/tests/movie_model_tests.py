from django.test import TestCase
from django.db.utils import IntegrityError

from api.movies.models import Movie
from .test_helper import TestHelper


class MovieModelTestCase(TestCase):
    """This class defines the test suit for the movie model"""

    def setUp(self):
        """Define the test client and other test variables"""
        image = TestHelper.get_tmp_image()
        self.movie_geners = TestHelper.get_movie_genres()
        self.movie_data = TestHelper.get_movie_data()

    def create_movie(self):
        """Helper method to create movie, set genre and return created movie object"""
        movie = Movie.objects.create(**self.movie_data)
        movie.genres.set(self.movie_geners)
        return movie

    def test_model_object_name_is_movie_name(self):
        movie = self.create_movie()
        self.assertEqual(str(movie), self.movie_data['name'])

    def test_model_can_create_a_movie(self):
        """Test the model can create a movie"""
        self.create_movie()
        self.assertEqual(Movie.objects.count(), 1)

    def test_model_can_prevent_duplicate_movie_name(self):
        """Test the model can prevent duplicate movie name"""
        self.create_movie()
        # creating another movie with same name, it should throw Integrity error
        self.assertRaises(IntegrityError, self.create_movie)

    def test_model_can_get_movie_by_name(self):
        """Test the model can get movie by name"""
        self.create_movie()
        movie = Movie.objects.get(name=self.movie_data['name'])
        self.assertEqual(movie.name, self.movie_data['name'])

    def test_model_can_get_movie_by_id(self):
        """Test the model can get movie by id"""
        movie = self.create_movie()
        self.assertEqual(Movie.objects.filter(id=movie.id).count(), 1)
