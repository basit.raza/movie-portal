from django.test import TestCase
from api.movies.models import Genre


class GenreModelTestCase(TestCase):
    """This class defines the test suit for the genre model."""

    def setUp(self):
        """Define the test client and other test variables"""
        self.genre_name = "Drama"
        self.genre = Genre(name=self.genre_name)

    def test_model_can_create_a_genre(self):
        """Test the genre model can create a genre"""
        old_count = Genre.objects.count()
        self.genre.save()
        new_count = Genre.objects.count()
        self.assertNotEqual(old_count, new_count)

    def test_model_can_get_a_genre_by_name(self):
        """Test the genre model can create a genre"""
        self.genre.save()
        genre = Genre.objects.get(name=self.genre_name)
        self.assertEqual(genre.name, self.genre_name)
