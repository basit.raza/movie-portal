from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from .test_helper import TestHelper
from api.movies.models import Movie, Comment


class CommentsViewTestCase(TestCase):

    def setUp(self):
        self.test_user = User.objects.create_user(username='dummy', password='dummypassowrd')
        self.movie_data = TestHelper.get_movie_data()
        self.movie_genres = TestHelper.get_movie_genres()
        self.test_movie = self.create_movie()

        self.comment_url = reverse("movie-comment-route-create")
        self.client = APIClient()
        self.client.login(username='dummy', password='dummypassowrd')

        data = {
            "comment": "This is test comment",
            "movie": self.test_movie.id
        }
        self.test_comment = self.client.post(self.comment_url, data, format="json")

    def create_movie(self):
        """Helper method to create movie, set genre and return created movie object"""
        movie = Movie.objects.create(**self.movie_data)
        movie.genres.set(self.movie_genres)
        return movie

    def test_api_can_post_comment(self):
        data = {
            "comment": "This is another test comment",
            "movie": self.test_movie.id
        }

        response = self.client.post(self.comment_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Comment.objects.count(), 2)

    def test_api_can_get_comment_by_movie(self):
        url = reverse("movie-comments-route-list", args=(self.test_movie.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
