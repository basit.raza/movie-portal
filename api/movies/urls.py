from django.urls import path, include
from rest_framework import routers

from api.movies.views import MovieViewSet, GenreViewSet, CommentCreateAPI, CommentsByMovieListAPI

router = routers.DefaultRouter()
router.register(r'movies', MovieViewSet, basename="movie-routes")
router.register(r'genres', GenreViewSet, basename="genres-routes")

urlpatterns = [
  path(r'movies/comments/post', CommentCreateAPI.as_view(), name="movie-comment-route-create"),
  path(r'movies/comments/<movie__id>/', CommentsByMovieListAPI.as_view(), name="movie-comments-route-list"),
  path('', include(router.urls))
]
