from rest_framework import viewsets, parsers
from rest_framework.response import Response
from rest_framework import generics, permissions
from api.movies.serializers import CommentSerializer
from api.movies.models import Comment


class CommentCreateAPI(generics.CreateAPIView):
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()
    permission_classes = (permissions.IsAuthenticated,)


class CommentsByMovieListAPI(generics.ListCreateAPIView):
    serializer_class = CommentSerializer
    queryset = Comment.objects.order_by('-created_at')

    def list(self, request, movie__id=None):
        queryset = Comment.objects.filter(movie__id=movie__id)
        serializer = CommentSerializer(queryset, many=True)
        return Response(serializer.data)
