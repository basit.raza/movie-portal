from rest_framework import viewsets

from api.movies.models import Genre
from api.movies.serializers import GenreSerializer


class GenreViewSet(viewsets.ModelViewSet):
    """This class defines the CRUD endpoints of movie api"""

    queryset = Genre.objects.all()
    serializer_class = GenreSerializer
