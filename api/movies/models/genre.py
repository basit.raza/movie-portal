from uuid import uuid4
from django.db import models


class Genre(models.Model):
    """This class represents the movie genre model."""
    id = models.UUIDField(primary_key=True, default=uuid4)
    name = models.CharField(max_length=255, unique=True, blank=False)

    class Meta:
        db_table = 'genres'

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return self.name