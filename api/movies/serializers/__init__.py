from .movie import MovieSerializer
from .genre import GenreSerializer
from .comment import CommentSerializer