from rest_framework import serializers
from api.movies.models import Genre


class GenreSerializer(serializers.ModelSerializer):
    """Serializer to map the Genre model instance into json format"""

    class Meta:
        """Meta class to map serializer's fields with the model fields"""
        model = Genre
        fields = ('id', 'name',)
        read_only_fields = ('id',)
