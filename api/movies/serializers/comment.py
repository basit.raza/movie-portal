from rest_framework import serializers
from api.movies.models import Comment


class CommentSerializer(serializers.ModelSerializer):
    """Serializer to map the Genre model instance into json format"""
    username = serializers.StringRelatedField(source="user", read_only=True)
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault(),
        write_only=True
    )

    class Meta:
        """Meta class to map serializer's fields with the model fields"""
        model = Comment
        fields = ('id', 'comment', 'username', 'created_at', 'user', 'movie',)
        read_only_fields = ('id', 'created_at',)
        extra_kwargs = {
            'movie': {'write_only': True}
        }

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)