Vue.component('auth-component', {
  props: ['type'],
  data: function () {
    return {
      errors: null,
      auth: {
        username: '',
        password: ''
      },
      message: null
    }
  },
  computed: {
    label () {
      if (this.type === 'signin') return 'Signin'
      return 'Signup'
    }
  },
  methods: {
    checkError(name) {
      if (this.errors) {
        if (this.errors[name]) {
          return this.errors[name]
        }
      }
    },
    submit () {
      if (this.type === 'signin') {
        this.doSignin()
      } else {
        this.doSignup()
      }
    },
    doSignin() {
      const self = this
      signin(this.auth).then(function (res) {
        setAuthToken(res.token)
        self.$emit('success')
      }, function (err) {
        self.errors = err
      })
    },
    doSignup() {
      const self = this
      signup(this.auth).then(function (res) {
        self.created = true
        setAuthToken(res.token)
        self.$emit('success')
      }, function (err) {
        self.errors = err
      })
    }
  },
  template: `
    <div class="component">
      <h2>{{label}}</h2>
      <div class="alert alert-warning" v-for="err in checkError('non_field_errors')">{{err}}</div>
      <form @submit.prevent="submit">
        <div class="field">
          <label>Username:</label>
          <input type="text" v-model="auth.username" />
          <div>
            <p class="error-message" v-for="err in checkError('username')">{{err}}</p>
          </div>
        </div>
        <div class="field">
          <label>Password:</label>
          <input type="password" v-model="auth.password" />
          <div>
            <p class="error-message" v-for="err in checkError('password')">{{err}}</p>
          </div>
        </div>
        <div class="action-control">
          <button>{{label}}</button>
        </div>
      </form>
    </div>
  `
})